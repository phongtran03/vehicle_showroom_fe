import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'], 
  template: `
     <h1>Welcome!</h1>
     <p>This is Home Component </p>
     `
})
export class HomeComponent {

}